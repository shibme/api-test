FROM golang AS build-env
WORKDIR /build
COPY . /build
#RUN go build -a -ldflags "-linkmode external -extldflags '-static' -s -w" -o app
#RUN go build -a -tags 'osusergo netgo static_build' -ldflags "-linkmode external -extldflags '-static' -s -w" -o app
RUN go build -a -tags 'osusergo netgo static_build' -ldflags '-w -extldflags "-static"' -o app

FROM scratch
COPY --from=build-env /build/app /app
ENV PORT=11111
ENTRYPOINT ["./app"]
module gitlab.com/shibme/api-test

go 1.16

require (
	github.com/gin-gonic/gin v1.7.1 // indirect
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.10 // indirect
)

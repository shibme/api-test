package main

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/shibme/api-test/controllers"
	"gitlab.com/shibme/api-test/models"
)

func main() {
	r := gin.Default()

	models.ConnectDataBase()

	r.GET("/books", controllers.ListBooks)
	r.POST("/books", controllers.CreateBook)

	r.Run()
}
